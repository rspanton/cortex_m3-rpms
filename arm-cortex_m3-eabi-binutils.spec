%define target arm-cortex_m-eabi

Name:           %{target}-binutils
Version:        2.24
Release:        1%{?dist}
Summary:        Cross Compiling GNU binutils targeted at %{target}

License:        GPLv3+
URL:            http://www.gnu.org/software/binutils/
Source0:        http://ftp.gnu.org/gnu/binutils/binutils-%{version}.tar.bz2

BuildRequires:  texinfo

%description
This is a Cross Compiling version of GNU binutils, which can be used to
assemble and link binaries for the %{target} platform,
instead of for the native %{_arch} platform.


%prep
%setup -q -c -n %{target}-binutils


%build
mkdir -p build
cd build

CFLAGS="$RPM_OPT_FLAGS" ../binutils-%{version}/configure --prefix=%{_prefix} \
        --libdir=%{_libdir} --mandir=%{_mandir} --infodir=%{_infodir} \
        --target=%{target} \
        --enable-interwork --enable-multilib --with-gnu-as --with-gnu-ld \
        --disable-nls

make %{?_smp_mflags}


%install
cd build
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# these are for win targets only
rm -f $RPM_BUILD_ROOT%{_mandir}/man1/%{target}-{dlltool,nlmconv,windres,windmc}.1

# we don't want these as this is a cross-compiler
rm -rf $RPM_BUILD_ROOT%{_infodir}
rm -f $RPM_BUILD_ROOT%{_libdir}/libiberty.a


%files
%doc binutils-%{version}/{COPYING,COPYING3,COPYING.LIB,COPYING3.LIB}

%dir %{_prefix}/%{target}

%{_bindir}/%{target}-*
%{_prefix}/%{target}/bin/*
%{_prefix}/%{target}/lib/*
%{_mandir}/man1/%{target}*.gz

%changelog
* Sun May 20 2012 Rob Spanton <rspanton@zepler.net> 2.22-1
- Initial release

